# nodejs_docs

#### nodejs 学习使用笔记

| 笔记列表                                                     |
| ------------------------------------------------------------ |
| [使用 nvm 安装 nodejs 环境](nvm_start.md)                    |
| [vue init webpack 创建新工程](vue_init_webpack.md)           |
| [vue 添加 element-ui](vue_add_elememtui.md)                  |
| [vue webpack打包单个文件无js\css的办法 HtmlWebpackInlineSourcePlugin](vue_add_HtmlWebpackInlineSourcePlugin.md) |
| [electron-vue + element-ui构建桌面应用](electron_start.md)   |
| [electron-vue 使用串口](electron_serialport.md)              |
|                                                              |









