# [electron-vue + element-ui构建桌面应用](electron_start.md)



最近需要用Node.js做一个桌面的应用，了解到electron可以用来做跨平台的桌面应用，而vue可以用来作为界面的解决方案，研究了一会儿如何把他们两个整合到一起使用，遇到了各种问题而放弃，毕竟作为一个非前端开发人员我的目的就是看这个东西能不能满足我的需求，而不想浪费太多的时间在上面，后来又看到了electron-vue，顾名思义就是将electron和vue整合到了一起直接使用，于是开始尝试搭建基于electron-vue的项目。

整个搭建过程对非前端开发来说还算可以，只是后面踩了两个坑费了一些时间，感觉应该大部分人都会遇到，因为我的一切步骤基本都是在官方文档的指导下完成的，如果出了问题那么很大可能是框架固有问题

### 根据官网给出的方法安装Node：

```
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get install -y nodejs
```

这个步骤需要curl，如果发现没有安装curl，可以用下面的命令安装一下：

```
sudo apt install curl
```

然后通过：

```
node -v
npm -v
```

查看一下node和npm是否成功安装上

正常情况下，可以使用 npm install [module-name] 来安装需要的模块

但是npm下载模块的时候经常有各种各样的报错，大多数报错原因都在于npm下载速度太慢了，可以用cnpm（[淘宝npm镜像](https://npm.taobao.org/)）的方式来安装

### 获取cnpm 安装vue-cli：

```
npm install -g cnpm --registry=https://registry.npm.taobao.org
```

接下来就像使用npm一样来使用cnpm即可，如果在使用cnpm的过程中看到报错信息有**permission denied**相关，直接在命令的前面加sudo就好了

后面经过一番折腾，发现还是用全局安装vue-cli脚手架的方式最好：

```
[sudo] cnpm install -g vue-cli
```

### 建立electron-vue项目

继续，通过vue init使用electron-vue模板来初始化项目：

```
vue init simulatedgreg/electron-vue my-project
```

这个过程会先下载模板，等待一会儿之后就会弹出下面的提示来配置项目，这里强烈建议把use ESLint给禁掉，因为这个东西真的是很严格，导致我后面遇到了很多代码格式的问题，还是比较恶心的，其他的像是unit test的东西也不是很了解，而且也用不到就都给禁用了，免得出什么幺蛾子，其他的都可以直接一路Enter使用默认的即可



### 安装库依赖

```shell
npm install
#或者
yarn
```

### 运行调试

```shell
npm run dev
#或者
yarn run dev
```

### 运行构建exe

```shell
npm run build
#或者
yarn run build
```





### electron-vue 集成element-ui

发现Element里面的UI还是很好看的，想要使用一下，于是开始集成element-ui：

同样按照官网的顺序（直接搜索element-ui就能找到官网了很方便）：

```shell
npm i element-ui -S
```

然后按照文档中[快速上手](https://element.eleme.cn/#/zh-CN/component/quickstart)的指示，在src/renderer/main.js中修改如下：

```
import Vue from 'vue'
import axios from 'axios'
import ElementUI from 'element-ui';  // 新添加
import 'element-ui/lib/theme-chalk/index.css';  //新添加

import App from './App'
import router from './router'
import store from './store'
Vue.use(ElementUI);  // 新添加

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.http = Vue.prototype.$http = axios
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  components: { App },
  router,
  store,
  template: '<App/>'
}).$mount('#app')
```

去src/renderer/components/LandingPage.vue中开始体验element-ui，直接把element-ui上的示例代码复制过来运行，结果发现一个很诡异的事情，el-table这个表格组件好像很特殊！它怎么都显示不出来，除了它以外其他的控件都能正常显示

找了几个做web的同事来大概搂了一眼，都说代码写的没有问题，奇怪了，于是开始各种搜索，终于在github上面找到了答案：https://github.com/SimulatedGREG/electron-vue/issues/361

根据electron-vue作者自己的回复，element-ui需要加入到白名单里面，需要修改`.electron-vue/webpack.renderer.config.js`

将：

```
let whiteListedModules = ['vue']
```

修改为：

```
let whiteListedModules = ['vue', 'element-ui']
```

然后表格控件就正常显示了！

 

偶然发现上面的process is not defined问题，在github上面也是有人提了issue的https://github.com/SimulatedGREG/electron-vue/issues/871，除了直接删除我截图中的代码的方式，还有一个在文件中添加代码的方式：

在.electron-vue/webpack.renderer.config.js和.electron-vue/webpack.web.config.js文件中都添加下面的代码：

![img](https://img2018.cnblogs.com/blog/1296826/201906/1296826-20190622182929986-882458575.png)







> > add this code solved the problem for me
> >
> > ```
> > templateParameters(compilation, assets, options) {
> >         return {
> >           compilation: compilation,
> >           webpack: compilation.getStats().toJson(),
> >           webpackConfig: compilation.options,
> >           htmlWebpackPlugin: {
> >             files: assets,
> >             options: options
> >           },
> >           process,
> >         };
> >       }
> > ```
>
> where add this your code ?

...electron-vue\webpack.renderer.config.js
...electron-vue\webpack.web.config.js

```
new HtmlWebpackPlugin({
      filename: 'index.html',
      template: path.resolve(__dirname, '../src/index.ejs'),
      templateParameters(compilation, assets, options) {
        return {
          compilation: compilation,
          webpack: compilation.getStats().toJson(),
          webpackConfig: compilation.options,
          htmlWebpackPlugin: {
            files: assets,
            options: options
          },
          process,
        };
      },
      minify: {
        collapseWhitespace: true,
        removeAttributeQuotes: true,
        removeComments: true
      },
      nodeModules: false
    }),
```



然后重新运行就可以了，这样就可以开始做简单的项目了

转载于:https://www.cnblogs.com/adorkable/p/11069923.html





### electron-vue 构建错误0：

```
 const tasks = new Listr(
        ^
SyntaxError: Identifier 'tasks' has already been declared
```

原因为安装multispinner

解决办法

```
npm install -D multispinner
```

然后再  .electron-vue/build.js文件 11行左右添加下面

```
const Multispinner = require('multispinner')
```

再将 .electron-vue/build.js文件37行左右修改tasks 成process_tasks 避免重名

```
  const tasks = ['main', 'renderer']
  const m = new Multispinner(tasks, {
    preText: 'building',
    postText: 'process'
  })
```



```
  const process_tasks = ['main', 'renderer']
  const m = new Multispinner(process_tasks, {
    preText: 'building',
    postText: 'process'
  })
```





### electron-vue 运行错误0：

WebpackDevServer的 compiler.hot = true 引起的

```
Failed to load resource: the server responded with a status of 404 (Not Found)
GET http://localhost:9080/__webpack_hmr 404 (Not Found)
```

将项目下的 .electron-vue/dev-runner.js文件

```js
   const server = new WebpackDevServer(
      compiler,
      {
        contentBase: path.join(__dirname, '../'),
        quiet: true,
        //hot: true,
        before (app, ctx) {
          // app.use(hotMiddleware)
          ctx.middleware.waitUntilValid(() => {
            resolve()
          })
        }
      }
    )
```





### electron-vue 错误1：

应用是看到了，可是里面有貌似是有报错啊： 

![img](https://img2018.cnblogs.com/blog/1296826/201906/1296826-20190622175013452-1427768496.png)

process is not defined... 什么鬼，我还什么都没动呢，进程没有被定义？于是又去了解了一下主进程和渲染进程的知识，大概研究了一下放弃了，有点浪费时间而且直觉告诉我不是这个问题

可以看到报错信息找到src/index.ejs中，既然process没有定义，那简单粗暴点，直接删除了用到process的这段看看行不行：

![img](https://img2018.cnblogs.com/blog/1296826/201906/1296826-20190622175551668-1984677068.png)

结果还真就可以了，运行起来后是这样的：

![img](https://img2018.cnblogs.com/blog/1296826/201906/1296826-20190622175807471-553396909.png)

如果觉得这个方式不靠谱，随便乱删原生代码感觉确实会有什么后遗症，可以看下面另一种解决方式。





























