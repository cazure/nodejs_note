# [vue init webpack 创建新工程](vue_init_webpack.md)



参考教程文章 

https://www.runoob.com/vue2/vue-install.html

https://blog.csdn.net/weixin_45627031/article/details/107846016



```bash
# 全局安装 vue-cli
npm install --global vue-cli

# 创建一个基于 webpack 模板的新项目
vue init webpack my-project

cd my-project
npm install
#调试
npm run dev
#打包发布
npm run build

```

