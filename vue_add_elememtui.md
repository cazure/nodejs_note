# [vue 添加 element-ui](vue_add_elememtui.md)



文章

https://element.eleme.cn/#/zh-CN/component/installation



```bash
npm i element-ui -S
```





#### 打开main.js,加入element-ui的js和css

```
import ElementUI from 'element-ui' //element-ui的全部组件
import 'element-ui/lib/theme-chalk/index.css'//element-ui的css
Vue.use(ElementUI) //使用elementUI
```

