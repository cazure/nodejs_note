# [使用 nvm 安装 nodejs 环境](nvm_start.md)

参考教程文章 http://nvm.uihtm.com/



### nvm是什么

nvm全英文也叫node.js version management，是一个nodejs的版本管理工具。nvm和n都是node.js版本管理工具，为了解决node.js各种版本存在不兼容现象可以通过它可以安装和切换不同版本的node.js。



### windows版本 nvm下载

- github地址:  https://github.com/coreybutler/nvm-windows/releases
- gitee地址:  https://gitee.com/cazure/nvm-windows

在上面下载最新版本,可以用下面的网址直接下载压缩包：

- [nvm 1.1.7-setup.zip](http://nvm.uihtm.com/nvm1.1.7-setup.zip)：安装版，推荐使用
- [nvm 1.1.7-noinstall.zip](http://nvm.uihtm.com/nvm1.1.7-noinstall.zip): 绿色免安装版，但使用时需进行配置。



### linux macos版本 nvm下载

- github地址: https://github.com/nvm-sh/nvm
- gitee地址:  https://gitee.com/cazure/nvm-linux/raw/master/install.sh






如果有或者新建完成后，我们通过官方的说明在终端中运行下面命令中的一种进行安装：

```
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.2/install.sh | bash
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.2/install.sh | bash
```

在安装完成后，也许你会在终端输入 `nvm` 验证有没有安装成功，这个时候你会发现终端打出 `Command not found`，其实这并不是没有安装成功，你只需要重启终端就行，再输入 `nvm` 就会出现 `Node Version Manager` 帮助文档，这表明你安装成功了。

这里需要注意的几点就是：

第一点 不要使用 `homebrew` 安装 `nvm`

第二点 关于 `.bash_profile` 文件。如果用户 `home` 目录下没有则新建一个就可以了，不需要将下面的两段代码写进去，因为你在执行安装命令的时候，系统会自动将这两句话写入 `.bash_profile` 文件中。

```
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
```

网络上我找了好多文章都是说在安装前先手动将下面这两句话写进去，经过测试是不正确的，并且会造成安装不成功，这一点需要注意一下。

```
export NVM_DIR="${XDG_CONFIG_HOME/:-$HOME/.}nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
```

第三点 保证 `Mac` 中安装了 `git`，一般只要你下载了 `Mac` 的 `Xcode` 开发工具，它是自带 `git` 的。



### nvm命令提示

- `nvm arch`：显示node是运行在32位还是64位。
- `nvm install <version> [arch]` ：安装node， version是特定版本也可以是最新稳定版本latest。可选参数arch指定安装32位还是64位版本，默认是系统位数。可以添加--insecure绕过远程服务器的SSL。
- `nvm list [available]` ：显示已安装的列表。可选参数available，显示可安装的所有版本。list可简化为ls。
- `nvm on` ：开启node.js版本管理。
- `nvm off` ：关闭node.js版本管理。
- `nvm proxy [url]` ：设置下载代理。不加可选参数url，显示当前代理。将url设置为none则移除代理。
- `nvm node_mirror [url]` ：设置node镜像。默认是https://nodejs.org/dist/。如果不写url，则使用默认url。设置后可至安装目录settings.txt文件查看，也可直接在该文件操作。
- `nvm npm_mirror [url]` ：设置npm镜像。https://github.com/npm/cli/archive/。如果不写url，则使用默认url。设置后可至安装目录settings.txt文件查看，也可直接在该文件操作。
- `nvm uninstall <version>` ：卸载指定版本node。
- `nvm use [version] [arch]` ：使用制定版本node。可指定32/64位。
- `nvm root [path]` ：设置存储不同版本node的目录。如果未设置，默认使用当前目录。
- `nvm version` ：显示nvm版本。version可简化为v。

------

### 安装node.js版本

`nvm list available` 显示可下载版本的部分列表

`nvm install latest`安装最新版本 ( 安装时可以在上面看到 node.js 、 npm 相应的版本号 ，不建议安装最新版本)

`nvm install` 版本号 安装指定的版本的nodejs

------

### 查看已安装版本

`nvm list`或`nvm ls`查看目前已经安装的版本 （ 当前版本号前面没有 * ， 此时还没有使用任何一个版本，这时使用 node.js 时会报错 ）

------

### 切换node版本

`nvm use`版本号 使用指定版本的nodejs （ 这时会发现在启用的 node 版本前面有 * 标记，这时就可以使用 node.js ）

------

### nvm常见问题

如果下载node过慢，请更换国内镜像源, 在 nvm 的安装路径下，找到 settings.txt，设置node_mirro与npm_mirror为国内镜像地址。下载就飞快了~~

root: D:\nvm
path: D:\nodejs
node_mirror: https://npm.taobao.org/mirrors/node/
npm_mirror: https://npm.taobao.org/mirrors/npm/









